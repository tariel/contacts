/*
http://net.tutsplus.com/tutorials/javascript-ajax/build-a-contacts-manager-using-backbone-js-part-1/
http://net.tutsplus.com/tutorials/javascript-ajax/build-a-contacts-manager-using-backbone-js-part-2/
*/

(function ($) {
  var contacts = [
    { name: "Contact 1", address: "1, a street, a town, a city, AB12 3CD", tel: "0123456789", email: "anemail@me.com", type: "family" },
    { name: "Contact 2", address: "1, a street, a town, a city, AB12 3CD", tel: "0123456789", email: "anemail@me.com", type: "family" },
    { name: "Contact 3", address: "1, a street, a town, a city, AB12 3CD", tel: "0123456789", email: "anemail@me.com", type: "friend" },
    { name: "Contact 4", address: "1, a street, a town, a city, AB12 3CD", tel: "0123456789", email: "anemail@me.com", type: "colleague" },
    { name: "Contact 5", address: "1, a street, a town, a city, AB12 3CD", tel: "0123456789", email: "anemail@me.com", type: "family" },
    { name: "Contact 6", address: "1, a street, a town, a city, AB12 3CD", tel: "0123456789", email: "anemail@me.com", type: "colleague" },
    { name: "Contact 7", address: "1, a street, a town, a city, AB12 3CD", tel: "0123456789", email: "anemail@me.com", type: "friend" },
    { name: "Contact 8", address: "1, a street, a town, a city, AB12 3CD", tel: "0123456789", email: "anemail@me.com", type: "family" }
  ];
  
  var Contact = Backbone.Model.extend({
    defaults: {
      photo: "img/placeholder.png",
      name: "",
      address: "",
      tel: "",
      email: "",
      type: ""
    }
  });
  
  // Define directory collection
  var Directory = Backbone.Collection.extend({
    model: Contact
  });
  
  var ContactView = Backbone.View.extend({
    tagName: "article",
    className: "contact-container",
    template: _.template($("#contactTemplate").html()),
    
    render: function () {      
      this.$el.html(this.template(this.model.toJSON()));      
      return this;
    },
    
    events: {
      "click button.delete": "deleteContact"
    },
    
    deleteContact: function () {
      var removedType = this.model.get("type").toLowerCase();
      
      // remove model
      this.model.destroy();
      
      // remove view
      this.remove();
      
      if (_.indexOf(directory.getTypes(), removedType) === -1) {
        directory.$el.find("#filter select").children("[value='" + removedType + "']").remove();
      }
    }
  });
  
  // Master view
  var DirectoryView = Backbone.View.extend({
    el: $("#contacts"),
    
    initialize: function () {
      this.collection = new Directory(contacts);
      
      this.render();
      this.$el.find('#filter').append(this.createSelect());
      
      this.on("change:filterType", this.filterByType, this);
      this.collection.on("reset", this.render, this);
      
      this.collection.on("add", this.renderContact, this);
      
      this.collection.on("remove", this.removeContact, this);
    },
    
    render: function () {
      this.$el.find("article").remove();
      
      _.each(this.collection.models, function (item) {
        this.renderContact(item);
      }, this);
    },
    
    renderContact: function (item) {
      var contactView = new ContactView({
        model: item  
      });
      
      // $el property is cached jQuery object
      // that Backbone creates automatically
      this.$el.append(contactView.render().el);
    },
    
    getTypes: function () {
      return _.uniq(this.collection.pluck("type"), false, function (type) {
        return type.toLowerCase();
      });
    },
    
    // Build types filter list
    createSelect: function () {
      var select = $("<select/>", {
          html: "<option>All</option>"
        });
      
      _.each(this.getTypes(), function (item) {
        var option = $("<option/>", {
          value: item.toLowerCase(),
          text: item.toLowerCase()
        }).appendTo(select);
      });
      return select;
    },
    
    // Define ui events
    events: {
      "change #filter select": "setFilter",
      "click #add": "addContact",
      "click #showForm": "showForm"
    },
    
    // Define filter handler
    setFilter: function (e) {
      this.filterType = e.currentTarget.value;
      this.trigger("change:filterType");
    },
    
    filterByType: function () {
      if (this.filterType === "all") {
        this.collection.reset(contacts);
        
        contactsRouter.navigate("filter/all");
      } else {
        this.collection.reset(contacts, { silent: true });
        
        var filterType = this.filterType,
            filtered = _.filter(this.collection.models, function (item) {
              return item.get("type") === filterType; 
        });
            
        this.collection.reset(filtered);
        
        contactsRouter.navigate("filter/" + filterType);
      }
    },
    
    // Define method for adding contacts
    addContact: function (e) {
      e.preventDefault();
      
      var newModel = {};
      $("#addContact").children("input").each(function (i, el) {
        if ($(el).val() !== "") {
          newModel[el.id] = $(el).val();
        }
      });
      
      contacts.push(newModel);
      
      if (_.indexOf(this.getTypes(), newModel.type) === -1) {
        this.collection.add(new Contact(newModel));
        this.$el.find("#filter").find("select").remove().end().append(this.createSelect());
      } else {
        this.collection.add(new Contact(newModel));
      }
    },
    
    removeContact: function (removedModel) {
      var removed = removedModel.attributes;
       
      if (removed.photo === "/img/placeholder.png") {
        delete removed.photo;
      }
      
      _.each(contacts, function (contact) {
        if (_.isEqual(contact, removed)) {
          contacts.splice(_.indexOf(contacts, contact), 1);  
        }  
      });
    },
    
    showForm: function () {
      this.$el.find("#addContact").slideToggle();
    }
  });
  
  // Define routes
  var ContactsRouter = Backbone.Router.extend({
    routes: {
      "filter/:type": "urlFilter"
    },
    
    urlFilter: function (type) {
      directory.filterType = type;
      directory.trigger("change:filterType");
    }
  });
  
  var directory = new DirectoryView();
  var contactsRouter = new ContactsRouter();
  Backbone.history.start();
  
} (jQuery));